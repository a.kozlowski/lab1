package types;

import java.util.Arrays;

public abstract class Sequence<T> extends Packet {
	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T[] buffer;
	public Sequence(int device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = buffer;
	}
	public Sequence(int device, String description, long date, int channelNr, String unit, double resolution,
			int len) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = (T[]) new Object[len];
	}
	@Override
	public String toString() {
		return "Sequence [channelNr=" + channelNr + ", unit=" + unit + ", resolution=" + resolution + ", buffer="
				+ Arrays.toString(buffer) + ", device=" + device + ", description=" + description + ", date=" + date
				+ "]";
	}
	
}
