package pierwszy;

import java.io.Serializable;

public abstract class Packet implements Serializable {
	protected int device;
	protected String description;
	protected long date;
	
	public Packet(int device, String description, long date) {
		super();
		this.device = device;
		this.description = description;
		this.date = date;
	}

	public int getDevice() {
		return device;
	}

	public void setDevice(int device) {
		this.device = device;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Packet [device=" + device + ", description=" + description + ", date=" + date + "]";
	}

	
}
