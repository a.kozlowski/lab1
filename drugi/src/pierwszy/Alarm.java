package pierwszy;

import enums.Direction;

public class Alarm extends Packet {
	private int channelNr;
	private int threshold;
	private Direction direction;
	
	public Alarm(int device, String description, long date, int channelNr, int threshold, Direction direction) {
		super(device, description, date);
		this.channelNr = channelNr;
		this.threshold = threshold;
		this.direction = direction;
	}
	public int getChannelNr() {
		return channelNr;
	}
	public void setChannelNr(int channelNr) {
		this.channelNr = channelNr;
	}
	public int getThreshold() {
		return threshold;
	}
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	@Override
	public String toString() {
		return "Alarm [channelNr=" + channelNr + ", threshold=" + threshold + ", direction=" + direction + ", device="
				+ device + ", description=" + description + ", date=" + date + "]";
	}
	
	
}
