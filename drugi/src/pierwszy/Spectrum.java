package pierwszy;

import java.util.Arrays;

import enums.Scale;

public class Spectrum<T> extends Sequence<T> {
	
	private Scale scaling;

	public Spectrum(int device, String description, long date, int channelNr, String unit, double resolution, int len) {
		super(device, description, date, channelNr, unit, resolution, len);
	}

	public Spectrum(int device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer) {
		super(device, description, date, channelNr, unit, resolution, buffer);
	}

	public Spectrum(int device, String description, long date, int channelNr, String unit, double resolution,
			T[] buffer, Scale scaling) {
		super(device, description, date, channelNr, unit, resolution, buffer);
		this.scaling = scaling;
	}

	public Scale getScaling() {
		return scaling;
	}

	public void setScaling(Scale scaling) {
		this.scaling = scaling;
	}

	@Override
	public String toString() {
		return "Spectrum [scaling=" + scaling + ", channelNr=" + channelNr + ", unit=" + unit + ", resolution="
				+ resolution + ", buffer=" + Arrays.toString(buffer) + ", device=" + device + ", description="
				+ description + ", date=" + date + "]";
	}
	
}
