package networking.Udp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import pierwszy.Packet;
import pierwszy.Sequence;
import sun.misc.IOUtils;
import pierwszy.TimeHistory;
import utils.FileOperations;

public class UDPClient {

  public static void main(String[] args) {
	  TimeHistory timeHistory = new TimeHistory(15, "opis", 20, 25, "jednostka", 966, 90);
	 
    /* try {
      // args contain server hostname
      if(args.length < 1) {
    	  System.out.println("Usage: UDPClient <server host name>");
    	  System.exit(-1);
      }
   	  byte[] buffer = new byte[1024];
      InetAddress aHost = InetAddress.getByName(args[0]);
      int serverPort = 9876;
      DatagramSocket aSocket = new DatagramSocket();
      Scanner scan = new Scanner(System.in);
      String line = "";
      while(true) {
    	  System.out.println("Enter your request (+,-,?,!)|nick|...|");
    	  if(scan.hasNextLine())
    		  line = scan.nextLine();
    	  DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
    	  aSocket.send(request);
    	  DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
    	  aSocket.receive(reply);
    	  System.out.println("Reply: " + new String(reply.getData(), 0, reply.getLength()));
      }
    } catch (SocketException ex) {
      Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
    } catch (UnknownHostException ex) {
      Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
    }*/
	  
	  
      int serverPort = 9876;
	  String filePath = System.getProperty("user.dir") + "/resources/testPlik.txt";
	 serializationToFile(timeHistory, filePath);
	 byte[] byteArray;
	try {
		DatagramSocket aSocket = new DatagramSocket();
		InetAddress aHost = InetAddress.getByName("localhost");
		byteArray = readDataFromFile(filePath);
		DatagramPacket datagramPacket = createDatagramPacket(byteArray, aHost, serverPort);
		sendPacket(aSocket, datagramPacket);
		receivePacket(aSocket, byteArray);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	
  } 
 
  private static byte[] readDataFromFile(String filePath) throws IOException {
	  
	  if (filePath != null ) {
		 return Files.readAllBytes(new File(filePath).toPath());
	  } else {
		  throw new IllegalArgumentException("Inappropriate file path!");
	  }
  }
  
  private static String convertByteArrayToString(byte[] byteArray) {
	  return Arrays.toString(byteArray); 
	 
  } 
  
  private static boolean serializationToFile (Object obj, String path) {
	 return FileOperations.serialize(obj, path);
	 
  }
  
  private static DatagramPacket createDatagramPacket(byte [] byteArray, InetAddress aHost, int serverPort) {
	  return new DatagramPacket(byteArray, byteArray.length, aHost, serverPort);
  }
  
  private static void sendPacket(DatagramSocket aSocket, DatagramPacket datagramPacket) throws IOException {
	  aSocket.send(datagramPacket);
  }
  
  private static void receivePacket(DatagramSocket aSocket,byte[] buffer ) throws IOException {
	  DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
	  aSocket.receive(reply);
  }
}
